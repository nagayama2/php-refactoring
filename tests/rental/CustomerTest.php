<?php

namespace rental;

require_once "../../src/rental/Customer.php";
require_once "../../src/rental/Rental.php";
require_once "../../src/rental/Movie.php";

use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    public function testStatement()
    {
        $expected = CustomerTest::normalize("
Rental Record for 太郎
\tスターウォーズ IV\t3.5
\tスターウォーズ V\t2
\tBLACK FOX\t6
\t空の青さを知る人よ\t3
\t仮面ライダー\t1.5
\tプリキュア\t1.5
Amount owed is 17.5
You earned 7 frequent renter points
");

        $customer = new Customer("太郎");
        $customer->addRental(new Rental(new Movie("スターウォーズ IV", Movie::REGULAR), 3));
        $customer->addRental(new Rental(new Movie("スターウォーズ V", Movie::REGULAR), 2));
        $customer->addRental(new Rental(new Movie("BLACK FOX", Movie::NEW_RELEASE), 2));
        $customer->addRental(new Rental(new Movie("空の青さを知る人よ", Movie::NEW_RELEASE), 1));
        $customer->addRental(new Rental(new Movie("仮面ライダー", Movie::CHILDRENS), 4));
        $customer->addRental(new Rental(new Movie("プリキュア", Movie::CHILDRENS), 3));

        $actual = $customer->statement();

        $this->assertEquals($expected, $actual);
    }

    private static function normalize(string $text) {
        return trim(str_replace("\r\n", "\n", $text));
    }
}
